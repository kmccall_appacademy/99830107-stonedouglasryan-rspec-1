def is_vowel(letter)
  "aeiou".include?(letter)
end

def pig_word(word)
  letters = word.chars
  return word + "ay" if is_vowel(letters.first)

  letters.each_with_index do |letter, idx|
    if is_vowel(letter) && letters[idx - 1] != "q"
      pl_word = letters[idx..-1].join("") + letters[0..idx - 1].join("") + "ay"
      return pl_word
    end
  end
end

def translate(str)
  words = str.split(" ")
  words.map! do |word|
    pig_word(word)
  end
  words.join(" ")
end
