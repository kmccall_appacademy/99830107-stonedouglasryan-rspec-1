def ftoc(temp)
  c_temp = ((temp - 32) * 5) /9
end

def ctof(temp)
  f_temp = ((temp * 9).fdiv(5)) + 32
end
