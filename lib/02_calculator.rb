def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  sum = 0
  arr.each{ |ele| sum += ele }
  sum
end

def multiply(num1, num2, num3=1)
  num1 * num2 * num3
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 1 if num < 2
  sum = 1
  (1..num).each do |current_num|
    sum *= current_num
  end
end
