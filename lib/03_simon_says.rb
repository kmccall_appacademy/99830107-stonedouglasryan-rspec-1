def echo(word)
  word
end

def shout(str)
  str.upcase
end

def repeat(word, times = 2)
  ([word] * times).join(" ")
end

def start_of_word(word, num)
  letters = word.split("")
  letters[0...num].join("")
end
def first_word(sent)
  words = sent.split(" ")
  words[0]
end

def titleize(title)
  title.capitalize!
  words = title.split(" ")
  words.each_with_index do |word, index|
    if index != 0
      word.capitalize! unless word.length <= 3 || word == "over"
    end
  end
  words.join(" ")
end
